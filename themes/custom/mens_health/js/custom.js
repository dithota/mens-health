/* Custom js files */
$(document).ready(function(){
	$('.answers').hide();
	$(".big-text").on('click', function(){
		$(".whats-it").slideToggle('slow');
		if($('.big-text b').hasClass('caret-right'))
		{
			$('.big-text b').removeClass('caret-right').addClass('caret');
		} else
		{
			$('.big-text b').removeClass('caret').addClass('caret-right');
		}
	});
	$('.questions').click(function(event){
                event.preventDefault();
                // create accordion variables
                var accordion = $(this);
                var accordionContent = $(accordion.parent().next().children().children());

                // toggle accordion content
               accordionContent.slideToggle('');
 
        if (accordion.hasClass('condense'))
        {
            $(accordion).removeClass('condense').addClass('expand');
			
        }
        else
        {
            $(accordion).addClass('condense').removeClass('expand');
        } 
    });

});