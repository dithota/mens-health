<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Men's Health - Home</title>
  </head>
  <?php
  // $node = node_load(7);
  // echo "<PRE>";
  // print_r($node);
  // echo "</pre>";
  $vocabulary = taxonomy_vocabulary_machine_name_load('life_style_terms');
	$terms = taxonomy_get_tree($vocabulary->vid);
	$courses = array();
	$termid = array();
	$energy = array();
	$performance = array();
	$wellbeing = array();
	foreach($terms as $term) {
		
	$courses[$term->tid] =array
    (
      'life_style' => $term->name,
      'desc' => $term->description,
    );
	$nodeid = db_query("select nid from {taxonomy_index} where tid=".$term->tid)->fetchAll();
			$termid[$term->tid] = $nodeid;
			foreach($nodeid as $node)
			{
				$node_title = node_load($node->nid);
				$node_term = taxonomy_term_load($node_title->field_life_style['und'][0]['tid']);
				if($node_term->name=='Energy') {
				$energy[$node->nid] =array
					(
					  'node_id' => 	$node->nid,
					  'term_name' => $node_term->name,
					  'title' => $node_title->field_packshot_nmae['und'][0]['value'],
					  'formula_desc' => $node_title->field__find_formula_description['und'][0]['value'],
					);
				}
				if($node_term->name=='Performance') {
				$performance[$node->nid] =array
					(
					  'node_id' => 	$node->nid,	
					  'term_name' => $node_term->name,
					  'title' => $node_title->field_packshot_nmae['und'][0]['value'],
					  'formula_desc' => $node_title->field__find_formula_description['und'][0]['value'],
					);
				}
				if($node_term->name=='Wellbeing') {
				$wellbeing[$node->nid] =array
					(
					  'node_id' => 	$node->nid,
					  'term_name' => $node_term->name,
					  'title' => $node_title->field_packshot_nmae['und'][0]['value'],
					  'formula_desc' => $node_title->field__find_formula_description['und'][0]['value'],
					);
				}
				
			}
	}
	
	
	?>
  <body>
   <nav class="navbar navbar-default">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".nav-collapse" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
		<!--<button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar tb-megamenu-button menuIstance-processed" type="button">
      <i class="fa fa-reorder" ></i>
    </button>-->
		<?php if ($logo): ?>
				<a class="navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Mens Health'); ?>" rel="home" id="logo">
				<img src="<?php print $logo; ?>" alt="<?php print t('Mens Health'); ?>" />        </a>
				<?php endif; ?> 
      </div>
		<!-- Collect the nav links, forms, and other content for toggling -->
      <div class="nav-collapse collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-left nav-bg">
          <?php print render($page['header']); ?>
        </ul>
      </div><!-- /.navbar-collapse --><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
    </nav>

    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h2>FIND YOUR FORMULA</h2>
          <p>Every man wants to be at his best.<br>Wellbeing. Energy. Performance. Every active lifestyle has its own challenges.<br> There’s a <strong>Men’s Health Lab</strong> product for every man.</p>
          <p>Find your <strong>Men’s Health Lab</strong> formula with our vitamin selector.</p>
          <h4>What’s your goal ?</h4>
        </div>
        <div class="the-range">
		<?php foreach($courses as $value): ?>
          <div class="formul-mainblcok">
            <div class="col-sm-2"><?php echo $value[life_style]; ?></div>
            <div class="col-sm-10">
              <label><?php echo $value[desc]; ?></label>
              <input type="radio" name="formula1" id="formula1" value="<?php echo $value[life_style]; ?>" />
            </div>
          </div>
		  <?php endforeach; ?>
		  
		  <div class="well-details">
		  <?php
		  $i = 0;
		  $len = count($wellbeing);
		  ?>
		  <?php foreach($wellbeing as $key=>$value): ?>
		  <?php 
		  if ($i == 0) {
			$termname = $value[term_name];
		  }
		  else  {
			$termname = '';  
		  }
		  ?>
	        <div class="col-sm-2"><?php echo $termname; ?></div>
            <div class="col-sm-6">
              <p><?php echo $value[formula_desc]; ?></p>
            </div>
            <div class="col-sm-4">
              <input type="radio" name="wellbe_details" id="wellbe_details" value="<?php echo $value[node_id]; ?>" />
              <label><?php echo $value[title]; ?></label><br>
             </div>
			 <div class="clearfix"></div> 
          <?php $i++; ?>
		  <?php endforeach; ?>
		  </div>     
		  <?php
		  $j = 0;
		  $len = count($energy);
		  ?>
		  <div class="energy-details" >
		  <?php foreach($energy as $key=>$value): ?>
		  <?php 
		  if ($j == 0) {
			$termname = $value[term_name];
		  }
		  else  {
			$termname = '';  
		  }
		  ?>
            <div class="col-sm-2"><?php echo $termname; ?></div>
            <div class="col-sm-6">
              <p><?php echo $value[formula_desc]; ?></p>
            </div>
            <div class="col-sm-4">
              <input type="radio" name="energy_details" id="energy_details" value="<?php echo $value[node_id]; ?>" />
              <label><?php echo $value[title]; ?></label><br>
               </div>
			   <div class="clearfix"></div>
			<?php $j++; ?>
		  <?php endforeach; ?>
          </div>
		  <?php
		  $k = 0;
		  $len = count($performance);
		  ?>
          <div class="perf-details">
		  <?php foreach($performance as $key=>$value): ?>
		  <?php 
		  if ($k == 0) {
			$termname = $value[term_name];
		  }
		  else  {
			$termname = '';  
		  }
		  ?>
            <div class="col-sm-2"><?php echo $termname; ?></div>
            <div class="col-sm-6">
              <p><?php echo $value[formula_desc]; ?></p>
            </div>
            <div class="col-sm-4">
              <input type="radio" name="energy_details" id="preformance_details" value="<?php echo $value[node_id]; ?>" />
              <label><?php echo $value[title]; ?></label><br>
               </div>
			   <div class="clearfix"></div>
			<?php $k++; ?>
		  <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
	<div id="ajax-image"></div>	
    <div id="ajax-data"></div>

	<?php if ($page['footer']): ?>
	<footer>
        <?php print render($page['footer']); ?>
		</footer>
 <!-- /#footer -->
    <?php endif; ?>

  <script>
	$(document).ready(function() {
	$('#ajax-image').hide();
	$( ".well-details" ).hide();
	$( ".energy-details" ).hide();
	$( ".perf-details" ).hide();

	var selectedVal = "";
	var selectedWellDetails = "";

	$(".well-details input[type='radio']").click(function() {
		var selectedWellDetails = $(".well-details input[type='radio']:checked");	
		var selectedWellVal = selectedWellDetails.val();
			
		$('#ajax-image').show();		
		 $.ajax({
			type: "POST",
			url: "ajax-formula",
			data: {"optionID" : selectedWellVal},
			complete: function(){
			$('#ajax-image').hide();
			},
			success: function( data ) {
					$("#ajax-data").html(data);	
					$("#ajax-data").show();					
			  }

		});		
	});
	$(".energy-details input[type='radio']").click(function() {
		var selectedWellDetails = $(".energy-details input[type='radio']:checked");	
		var selectedWellVal = selectedWellDetails.val();
		$('#ajax-image').show();
		 $.ajax({
			type: "POST",
			url: "ajax-formula",
			data: {"optionID" : selectedWellVal},
			complete: function(){
			$('#ajax-image').hide();
			},
			success: function( data ) {
					$("#ajax-data").html(data);	
					$("#ajax-data").show();					
			  }

		});		
	});
	$(".perf-details input[type='radio']").click(function() {
		
		var selectedWellDetails = $(".perf-details input[type='radio']:checked");	
		var selectedWellVal = selectedWellDetails.val();
		$('#ajax-image').show();
		 $.ajax({
			type: "POST",
			url: "ajax-formula",
			data: {"optionID" : selectedWellVal},
			complete: function(){
			$('#ajax-image').hide();
			},
			success: function( data ) {
					$("#ajax-data").html(data);	
					$("#ajax-data").show();					
			  }

		});		
	});
	

	$(".formul-mainblcok input[type='radio']").click(function() {
			
	var selected = $(".formul-mainblcok input[type='radio']:checked");
	
  if (selected.length > 0) {
    selectedVal = selected.val();
	
	if(selectedVal=='Wellbeing')
	{
		  
		$( ".well-details" ).toggle( "slow");
		$( ".perf-details" ).hide();
		$( ".energy-details" ).hide();
		$("#ajax-data").hide();
		
	}
	if(selectedVal=='Energy')
	{
		$( ".energy-details" ).toggle( "slow");
		$( ".well-details" ).hide();
		$( ".perf-details" ).hide();
		$("#ajax-data").hide();
		
	}
	if(selectedVal=='Performance')
	{
		$( ".perf-details" ).toggle( "slow");
		$( ".well-details" ).hide();
		$( ".energy-details" ).hide();
		$("#ajax-data").hide();
	}
  }
	});
});
</script>

