<?php 
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
 global $user;
 global $base_url;
 $path_to_theme =  $base_url . '/' . path_to_theme();

//print_r($main_menu->title);
?>

<body>
<nav class="navbar navbar-default">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".nav-collapse" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
		<!--<button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar tb-megamenu-button menuIstance-processed" type="button">
      <i class="fa fa-reorder" ></i>
    </button>-->
		<?php if ($logo): ?>
				<a class="navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Mens Health'); ?>" rel="home" id="logo">
				<img src="<?php print $logo; ?>" alt="<?php print t('Mens Health'); ?>" />        </a>
				<?php endif; ?> 
      </div>
		<!-- Collect the nav links, forms, and other content for toggling -->
      <div class="nav-collapse collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-left nav-bg">
          <?php print render($page['header']); ?>
        </ul>
      </div><!-- /.navbar-collapse --><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
    </nav>
	
	
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="home-banner">
              <img style="margin-left: 10px;" src="<?php print $path_to_theme . '/images/new.png'; ?>" title="New" />
              <h1><span class="text-extnd">THE ULTIMATE VITAMINS FOR MEN</span><br>from the world's No.1 Men's Magazine</h1>
              <img src="<?php print $path_to_theme . '/images/home-banner.jpg'; ?>" title="Mens Health" />
            <div class="text-right">
              <a href="./products"><button class="explore-btn">EXPLORE FULL RANGE</button></a>
            </div>
            <div class="btn-n-img">
              <img src="<?php print $path_to_theme . '/images/fullrange-bg.png'; ?>" title="Mens Health" />
            </div>
          </div>
            <h2 class="release-best">#ReleaseYourBest</h2>
        </div>
      </div>
    </div>
	<div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="products">
            <h3>FIND YOUR<br>FORMULA</h3>
            <div class="product-img find-formula">
			<?php print render($page['triptych_first']); ?>         
            </div>
            <a href="find-your-formula"><button class="btn btn-green">Learn more</button></a>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="products">
            <h3>LEARN MORE ABOUT MH LAB</h3>
            <div class="product-img">
              <?php print render($page['triptych_middle']); ?>
            </div>
            <a href="content/ultimate-vitamin-range-men"><button class="btn btn-green">Learn more</button></a>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="products">
            <h3>I WANT TO KNOW</h3>
            <div class="product-img">
              <?php print render($page['triptych_last']); ?>
            </div>
            <a href="faqs"><button class="btn btn-green">Learn more</button></a>
          </div>
        </div>
      </div>
    </div>
	<div class="container">
      <div class="row">
        <div class="col-sm-12 prod-avail">
          <span>Available from</span>
          <a href="http://www.boots.com" target="_blank"><span class="boots"></span></a>
          <a href="#"><span class="marrisons"></span></a>
          <a href="#"><span class="tesc"></span></a>
          <a href="#"><span class="holland"></span></a>
        </div>
      </div>
    </div>
	<footer>
	<?php print render($page['footer']); ?>
	</footer>
	</body>