<?php

/**
 * @file
 * template.php
 */

/**
 * Implements theme_menu_tree().
 */
function mens_health_menu_tree(&$variables) {
  global $language;
  global $cur_level;
  
  if($variables['theme_hook_original'] == 'menu_tree__main_menu') {
	if($cur_level == 0) {
      return '<ul class="nav navbar-nav">' . $variables['tree'] . '</ul>';
	}
	elseif($cur_level == 1) {
	  return '<ul class="dropdown-menu multi-level">' . $variables['tree'] . '</ul>';
	}
	else{
		return '<ul class="dropdown-menu">' . $variables['tree'] . '</ul>';
	}
  }
}

function mens_health_menu_link(&$variables) {
  $element = $variables['element'];
  $sub_menu = '';
  global $cur_level;
  $element['#localized_options']['html'] = TRUE;
  
  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }

  if($element['#original_link']['depth'] == 1) {
	  $element['#attributes']['class'][] = 'dropdown-level-1';
	   $cur_level = 0;
	   if($element['#original_link']['has_children']) {
	     $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
	     //$element['#localized_options']['attributes']['data-toggle'] = 'dropdown';
	     $element['#title'] = $element['#title'] . ' <span class="glyphicon custom-right-down hide-desk pull-right visible-xs"></span>';
	   }
  }
  elseif($element['#original_link']['depth'] == 2)  {
	  $cur_level = 1;
	  $element['#attributes']['class'][] = 'dropdown-submenu';
	  $element['#title'] = '<span class="glyphicon glyphicon-menu-right hide-desk"> </span> ' . $element['#title'];
  }
  else {
	  $cur_level++;
  }
  if(in_array('active-trail', $element['#attributes']['class'])) {
	  $element['#attributes']['class'][] = 'active';
  }
  $output = l($element['#title'] , $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

