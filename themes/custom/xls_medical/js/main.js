
initDropdown();
function initDropdown(){
	
	//check 2 rows of text in ul dropdown from subnav
	jQuery('.withSubMenu').each(function(){
		var ul = jQuery(this).find('ul');
		//check line height smaller than height ? => then text is on 2 rows and top from submenu (ul) == line-height
		if(parseInt(jQuery(this).height()) > parseInt(jQuery(this).css('line-height'))){
			var top = parseInt(jQuery(this).css('line-height'));
			ul.css('top', top);
		}
		
	});
	
	var firstTimeHovered = true;
   
	var fixedBGHeight = 130;
	
	jQuery('.withSubMenu').not('.active').bind('mouseover', function(){
		jQuery(this).children('ul').show();
		
	}).bind('mouseleave',function(){
        jQuery('#subnav > li > a').each(function(){
            jQuery(this).css('z-index','5');
		});
        jQuery(this).children('ul').hide();
		if(isThisChrome() && firstTimeHovered){
			var klasse = jQuery('.withSubMenu').not('.active').parent('ul').attr('class');
			if(klasse.indexOf('large') != -1){
				jQuery('.withSubMenu').not('.active').children('ul').css('margin-top', '-3px');
				firstTimeHovered = false;
			}
		}
    });
	
	checkChromeSafari();
}

function isThisChrome(){
	return ( /chrome/.test(navigator.userAgent.toLowerCase()));
}

function checkChromeSafari(){
	//var isChrome = (jQuery.browser.safari && /chrome/.test(navigator.userAgent.toLowerCase()));
	var isChrome = isThisChrome();
	var klasse = jQuery('.withSubMenu').not('.active').parent('ul').attr('class');
	if( klasse &&  klasse.indexOf('large') != -1 && !isChrome){
		jQuery('.withSubMenu').not('.active').children('ul').css('margin-top', '25px');
	}
	
	if(isThisChrome()&& klasse && klasse.indexOf('large') != -1){
		jQuery('.withSubMenu').not('.active').children('ul').css('margin-top', '25px');
	}
	if( klasse && (klasse.indexOf('da') != -1 || klasse.indexOf('nb') != -1) &&  klasse.indexOf('large') != -1){
		
		jQuery('.withSubMenu').not('.active').each(function(){
			if(jQuery(this).find('a').html().indexOf(' kontrolleres portioner og ') != -1 || jQuery(this).find('a').html().indexOf('Slik kontrollerer du dine porsjoner og ') != -1){
				jQuery(this).children('ul').css('margin-top', '-3px');
			}
		});
	}
}

jQuery( document ).ready(function() {	
		
		jQuery("li.dropdown-level-1").on("click", function(){
		});
           
		   
		
		jQuery("#mobile-submenu-container").on("click", function () {
				jQuery("ul.dropdown-menu-custom").slideToggle();
		});
		jQuery(document).click(function(e){
			var container=jQuery("#mobile-submenu-container");
			if(!container.is(e.target) && container.has(e.target).length===0){
				jQuery("ul.dropdown-menu-custom").slideUp();
			}			
		});
			
		jQuery('#main-menu').on('shown.bs.collapse', function() {
		  jQuery('#navbar-hamburger').addClass('hidden');
		  jQuery('#navbar-close').removeClass('hidden');    
		}).on('hidden.bs.collapse', function() {
		  jQuery('#navbar-hamburger').removeClass('hidden');
		  jQuery('#navbar-close').addClass('hidden');        
		});
			
		jQuery('.youtube-play').click(function () {
            var src = jQuery(this).data("src");      
            jQuery('#myModal iframe').attr('src', src);
        });

        jQuery('#myModal button').click(function () {
            jQuery('#myModal iframe').removeAttr('src');
        });
  
  
});


