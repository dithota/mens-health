	<header>
		<div class="container custom-center">
		    <div class="row">
				<div class="col-md-12 top hidden-xs hidden-sm">
				<?php if ($logo): ?>
					<div class="logo">
						<a id="logo" title="<?php print $site_name; ?>"	href="<?php print $front_page; ?>"></a>
					</div>
				<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="container custom-center visible-sm">
			<div class="row">
				<div class="col-md-12 top hidden-xs">
				<?php if ($logo): ?>
					<div class="logo">
						<a id="logo" title="<?php print $site_name; ?>"	href="<?php print $front_page; ?>"></a>
					</div>
				<?php endif; ?>		  
				</div>
			</div>
		</div>	
		<?php if (!empty($page['header'])): ?>
			<?php print render($page['header']); ?>
		<?php endif; ?>
	</header>
		<nav class="navbar navbar-default custom-nav">
			<div class="container custom-center menu-container">
			  <div class="navbar-header">
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                   <div id="navbar-hamburger">
			         <span class="sr-only">Toggle navigation</span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
				   </div>
				   <div class="hidden" id="navbar-close">
				     <span class="glyphicon glyphicon-remove"></span>
				   </div>
			    </button>
				<?php
				global $base_path;
				$theme =  $base_path . drupal_get_path('theme', 'mens_health');
				?>
			    <a href="#" class="navbar-brand visible-xs"><img src="<?php echo $theme ?>/images/m-logo.png"></a>
			</div>
			<div id="main-menu" class="collapse navbar-collapse">
				<?php if (!empty($page['navigation'])): ?>
					<?php print render($page['navigation']); ?>
				<?php endif; ?>
			</div>
			<?php
				global $language;
				$attributes = array('id' => array('subnav'),'class' => array('hidden-xs'));
				print theme('links', array('links' => menu_navigation_links('menu-primary' , 1), 'attributes' => $attributes));
			?>
		    </div>
			<div class="sbox heiBanner">
				<?php if (!empty($title_prefix)): print render($title_prefix);  endif; ?>
				<?php if (!empty($title)): ?><h1><?php print $title; ?></h1> <?php endif; ?>
				<?php if (!empty($title_suffix)): print render($title_suffix);  endif; ?>
				<?php if (!empty($page['top'])):  print render($page['top']); endif; ?>
			</div>
		</nav>
        <div class="container visible-xs">
		    <div class="row">
			    <div id="mobile-submenu">
				    <div id="mobile-submenu-container">
					  <a href="javascript:void(0);">
					  <?php if (!empty($title_prefix)): print render($title_prefix);  endif; ?>
					  <?php if (!empty($title)): ?><?php print $title; ?> <?php endif; ?>
					  <?php if (!empty($title_suffix)): print render($title_suffix);  endif; ?>
					  <?php if (!empty($page['top'])):  print render($page['top']); endif; ?>
					  <b class="caret"></b>
					  </a>
					  <?php
						global $language;
						$attributes = array('class' => array('dropdown-menu-custom'));
						print theme('links', array('links' => menu_navigation_links('menu-primary' , 1), 'attributes' => $attributes));
					  ?>
					</div>					
			    </div>
		    </div>
	    </div>
	
	<div class="container custom-center content-bg padd-right-reset padd-left-reset">
	<div class="row">
		<div class="col-md-3 hidden-xs hidden-sm padd-right-reset">
		<div>
		<div class="left-box-content">
		<div class="en">
			  <?php if (!empty($page['sidebar_top'])):  print render($page['sidebar_top']); endif; ?>
			  <?php if (!empty($page['sidebar_middle'])):  print render($page['sidebar_middle']); endif; ?>
			  <?php if (!empty($page['sidebar_bottom'])):  print render($page['sidebar_bottom']); endif; ?>
		</div>
		</div>
		</div>
		</div>
		<div class="col-md-9 col-xs-12 padd-left-reset border-left">
		  <div class="content-right">
			<div class="clear"></div>
			<?php if (!empty($messages)): print $messages; endif;?>
			<?php if (!empty($tabs)): ?>
				<?php print render($tabs); ?>
			<?php endif; ?>
			<?php if (!empty($action_links)): ?>
				<ul class="action-links"><?php print render($action_links); ?></ul>
			<?php endif; ?>
			<?php  if (!empty($page['content_top'])): print render($page['content_top']); endif; ?>
			<?php  if (!empty($page['content'])): print render($page['content']); endif; ?>
			<?php  if (!empty($page['content_bottom'])): print render($page['content_bottom']); endif; ?>
			
		  </div>
		</div>
		<div class="clear"></div>
	</div>
	</div>
	<footer>
		<div class="container custom-center footer-bg">
			<div class="row">
				<?php if (!empty($page['footer_left'])): print render($page['footer_left']);  endif; ?>
				<?php if (!empty($page['footer_right'])): print render($page['footer_right']);  endif; ?>
			</div>
		</div>
		<?php if (!empty($page['footer'])): print render($page['footer']);  endif; ?>
	</footer>
