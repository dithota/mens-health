api = 2
core = 7.x
projects[drupal][version] = 7.39

; Patches for Core profile
projects[drupal][patch][] = "https://www.drupal.org/files/issues/drupal-inheritable-profiles-2067229-47.patch"
projects[drupal][patch][] = "https://drupal.org/files/issues/install-redirect-on-empty-database-728702-36.patch"
projects[drupal][patch][] = "https://www.drupal.org/files/undefined-menu-translate-notice-951098-50.patch"
includes[] = drupal-org.make