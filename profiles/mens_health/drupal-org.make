; Drupal.org release file.
core = 7.x
api = 2

defaults[projects][subdir] = contrib

; Basic contributed modules.
projects[ctools][version] = 1.7
projects[views][version] = 3.11
projects[libraries][version] = 2.2
projects[token][version] = 1.6

; Features modules
projects[features][version] = 2.6
projects[features][patch][2143765] = "http://drupal.org/files/issues/features-fix-modules-enabled-2143765-1.patch"
projects[features][patch][2479803] = "https://www.drupal.org/files/issues/ignore_hidden_modules-2479803-1.patch"
projects[features][patch][2534138] = "https://www.drupal.org/files/issues/2534138-field-base-exception-catch-1.patch"
projects[strongarm][version] = 2.0


; Sitebuilding modules
projects[views_bulk_operations][version] = 3.3
projects[taxonomy_menu][version] = 1.5
projects[views_slideshow][version] = 3.1
projects[menu_block][version] = 2.7
projects[entity][version] = 1.6
projects[imce][version] = 1.9
projects[ckeditor][version] = 1.16
projects[ds][version] = 2.10
projects[pathauto][version] = 1.2
projects[service_links][version] = 2.3
projects[mollom][version] = 2.14
projects[blockreference][version] = 2.2
projects[uuid][version] = 1.0-alpha6
projects[field_collection][version] = 1.0-beta8
projects[uuid_features][version] = 1.0-alpha4
projects[customerror][version] = 1.4
projects[field_group][version] = 1.4
projects[features_extra][version] = 1.0-beta1
;projects[features_extra][patch][] = "https://www.drupal.org/files/1901116-37-features_extra-install_profile.patch"

;Performance
projects[lazyloader][version] = 1.4
projects[imageapi_optimize][version] = 1.2
projects[cache_warmer][version] = 4.1

; Base theme.
projects[bootstrap][version] = 3.0

;Security
projects[seckit][version] = 1.9
projects[securepages][version] = 1.0-beta2
projects[eu_cookie_compliance][version] = 1.14

;Other modules
projects[google_tag][version] = 1.0
projects[google_analytics][version] = 2.1
projects[jquery_update][version] = 3.0-alpha2
projects[menu_icons][version] = 3.0-beta4
projects[bundle_copy][version] = 1.1
projects[color_field][version] = 1.7

;Non production modules
projects[devel][version] = 1.5
projects[coder][version] = 2.5
projects[backup_migrate][version] = 3.1
projects[module_filter][version] = 2.0


;SEO modules
projects[globalredirect][version] = 1.5
projects[site_verify][version] = 1.1
projects[xmlsitemap][version] = 2.2
projects[page_title][version] = 2.7
projects[match_redirect][version] = 1.0
projects[metatag][version] = 1.6
projects[search404][version] = 1.3
projects[seo_checklist][version] = 4.1
projects[seo_checker][version] = 1.8
projects[checklistapi][version] = 1.2

;mens_health contributed moduels
projects[better_exposed_filters][version] = 3.2
projects[chosen][version] = 2.0-beta4
projects[faq][version] = 1.1
projects[fontawesome][version] = 2.1
projects[imageblock][version] = 1.3
projects[tb_megamenu][version] = 1.0-rc2
projects[views_php][version] = 1.0-alpha1
projects[views_row_selector][version] = 1.0-alpha1
projects[weight][version] = 2.5