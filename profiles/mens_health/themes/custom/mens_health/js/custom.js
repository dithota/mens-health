/* Custom js files */
$(document).ready(function(){
	
	$('.answers').hide();
	//$('.prod-disp.Wellbeing').hide();
	//$('.prod-disp.Energy').hide();
	//$('.prod-disp.Performance').hide();
	$('.whatsinit-details').hide();
	$('.whatsinit').click('tabsselect', function (event, ui) {
		$('.formulated-details').hide();
		$('.whatsinit').addClass('active');
		$('.formulated').removeClass('active');
		$(".whatsinit-details").slideToggle('slow');
	});
	$('.formulated').click('tabsselect', function (event, ui) {
		$('.whatsinit').removeClass('active');
		$('.formulated').addClass('active');
		$('.whatsinit-details').hide();
		$('.formulated-details').slideToggle('slow');
		//$(".whatsinit-details").slideToggle('slow');
	});
		

	/*$(".big-text").on('click', function(){
		$(".whats-it").slideToggle('slow');
		if($('.big-text b').hasClass('caret-right'))
		{
			$('.big-text b').removeClass('caret-right').addClass('caret');
		} else
		{
			$('.big-text b').removeClass('caret').addClass('caret-right');
		}
	});*/
	//toogle for products page  
	
	/*$(".prod-disp .Wellbeing").on('click', function(){
		$('html, body').animate({
        scrollTop: $('.Wellbeing').offset().top
    }, 'slow');
		$(".prod-disp.Wellbeing").slideToggle('slow');
	});
	$(".prod-disp .Energy").on('click', function(){
		$(".prod-disp.Energy").slideToggle('slow');
		  $('html, body').animate({
        scrollTop: $('.Energy').offset().top
    }, 'slow');
	});
	$(".prod-disp .Performance").on('click', function(){
		$('html, body').animate({
        scrollTop: $('.Performance').offset().top
    }, 'slow');
		$(".prod-disp.Performance").slideToggle('slow');
	});*/
	$('.questions').click(function(event){
                event.preventDefault();
                // create accordion variables
                var accordion = $(this);
                var accordionContent = $(accordion.parent().next().children().children());

                // toggle accordion content
               accordionContent.slideToggle('');
 
        if (accordion.hasClass('condense'))
        {
            $(accordion).removeClass('condense').addClass('expand');
			
        }
        else
        {
            $(accordion).addClass('condense').removeClass('expand');
        } 
    });
	//custom menu js
	$('.producticons.wellbeing').show();
	$('.producticons.energy').hide();
	$('.product-range-sub-menu').hide();
	$('.product-range-sub-menu').hide();
	
	$( ".product-range" ).mouseover(function(event) {
	$( ".product-range .mega-dropdown-inner" ).hide();
	$('.product-range-sub-menu').show();
	});
	$('.product-range-sub-menu').mouseleave(function(event){
		$('.product-range-sub-menu').hide();
	});
	$('.faq-menu, .your-formula, .about').mouseover(function(event){
		$('.product-range-sub-menu').hide();
	});
	$('.productrangeicon .WELLBEING').click(function(event){
		$('.producticons.wellbeing').show();
		$('.producticons.energy').hide();
	});
	$('.productrangeicon .ENERGY').click(function(event){
		$('.producticons.wellbeing').hide();
		$('.producticons.energy').show();
	});
	
});