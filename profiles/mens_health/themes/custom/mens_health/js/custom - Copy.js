/* Custom js files */
$(document).ready(function(){
	$(".big-text").on('click', function(){
		$(".whats-it").slideToggle('slow');
		if($('.big-text b').hasClass('caret-right'))
		{
			$('.big-text b').removeClass('caret-right').addClass('caret');
		} else
		{
			$('.big-text b').removeClass('caret').addClass('caret-right');
		}
	});

    $('.questions').click(function(event){
    	event.preventDefault();
    	// create accordion variables
    	var accordion = $(this);
    	var accordionContent = accordion.next('.answers');
    	var accordionToggleIcon = $(this).children('.toggle-icon');

    	// toggle accordion link open class
    	accordion.toggleClass("open");
    	// toggle accordion content
    	accordionContent.slideToggle('');

    	if (accordion.hasClass("open")) {
    		accordionToggleIcon.html("<i class='caret'></i>");
    	} else {
    		accordionToggleIcon.html("<i class='caret-right'></i>");
    	}
    });

});