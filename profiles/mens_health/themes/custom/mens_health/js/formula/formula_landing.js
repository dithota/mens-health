$(document).ready(function() {
	$('.vitamin-selector').hide();
	$('.products-icon').show();
	$('#Energy.Release-details').hide();
	$(".products-bg, .products-icon").click(function() {
		var selectedWellVal = $(this).attr('id');
		var selectedClass = $(this).attr('class');
		if($(this).hasClass('Anti-Fatigue'))
		{
		$('.vitamin-selector').hide();
		$('.products-icon').show();
		$('.Anti-Fatigue.normal').css('display','none');
		$('.Anti-Fatigue').next(".vitamin-selector").show();
		}
		else if($(this).hasClass('Muscle Support')) {
		$('.vitamin-selector').hide();
		$('.products-icon').show();
		$('.Muscle.Support').next(".vitamin-selector").show();
		$('.Muscle.Support.normal').css('display','none');
		}
		else if($(this).hasClass('Brain Health')) {
		$('.vitamin-selector').hide();
		$('.products-icon').show();		
		$('.Brain.Health.normal').hide();			
		$('.Brain.Health').next(".vitamin-selector").show();
		}
		else if($(this).hasClass('Nails')) {
		$('.vitamin-selector').hide();
		$('.products-icon').show();			
		$('.Nails.normal').hide();			
		$('.Nails').next(".vitamin-selector").show();
		}
		else if($(this).hasClass('Immune Support')) {
		$('.vitamin-selector').hide();
		$('.products-icon').show();			
		$('.Immune.Support.normal').hide();			
		$('.Immune.Support').next(".vitamin-selector").show();
		}
		else if($(this).hasClass('Energy')) {
		$('.vitamin-selector').hide();
		$('.products-icon').show();			
		$('.Energy.normal').hide();			
		$('.Energy').next(".vitamin-selector").show();
		}
		else if($(this).hasClass('General Health')) {
		$('.vitamin-selector').hide();
		$('.products-icon').show();
		$('.General.Health.normal').hide();			
		$('.General.Health').next(".vitamin-selector").show();
		}
		else if($(this).hasClass('Bones')) {
		$('.vitamin-selector').hide();	
		$('.products-icon').show();
		$('.Bones.normal').hide();			
		$('.Bones').next(".vitamin-selector").show();
		}
		else if($(this).hasClass('Heart Health')) {
		$('.vitamin-selector').hide();	
		$('.products-icon').show();
		$('.Heart.Health.normal').hide();			
		$('.Heart.Health').next(".vitamin-selector").show();
		}
		else {
			$('.vitamin-selector').hide();
			$('.products-icon').show();
		}

		 $.ajax({
			type: "POST",
			url: "ajax-formula",
			data: {"optionID" : selectedWellVal,"optionClass" : selectedClass},
			complete: function(){
			//$('#ajax-image').hide();
			},
			success: function( data ) {
					$("#ajax-data").html(data);				
					$("#ajax-data").show();					
			  }

		});		
	});
	
	
	});