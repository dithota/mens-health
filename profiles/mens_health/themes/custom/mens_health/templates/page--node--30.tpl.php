<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Men's Health - Home</title>
  </head>
  <?php
   global $base_url;
	$path_to_theme =  $base_url . '/' . path_to_theme();
	$vit_file = file_load(variable_get('vitamin-header-image'));
	$vitamin_uri = $vit_file->uri;
	$vitamin_image_url = file_create_url($vitamin_uri);
	$vocabulary = taxonomy_vocabulary_machine_name_load('vitamin_selector');
	$terms = taxonomy_get_tree($vocabulary->vid);
	
	$vitamins = array();
	$termid = array();
	$products = array();
	foreach($terms as $term) {
	$term_image = taxonomy_term_load($term->tid);
	$image_items = field_get_items('taxonomy_term', $term_image, 'field_vitamin_image');
	$image_active_items = field_get_items('taxonomy_term', $term_image, 'field_vitamin_active_image');
	$active_uri = $image_active_items[0]['uri'];
	$uri = $image_items[0]['uri'];
	
	$external_url = file_create_url($uri);
	$external_active_url = file_create_url($active_uri);

	$vitamins[$term->tid] =array
    (
      'vitamin_name' => $term->name,
      'desc' => $term->description,
	  'vitamin_image' => $external_url,
	  'vitamin_active_image' => $external_active_url,
	  'term_id' => $term->tid,
    );
	$nodeid = db_query("select nid from {taxonomy_index} where tid=".$term->tid)->fetchAll();
	$termid[$term->tid] = $nodeid;
			foreach($nodeid as $node)
			{
				$node_title = node_load($node->nid);
				$node_term = taxonomy_term_load($node_title->field_life_style['und'][0]['tid']);
				$products[$node->nid] =array
					(
					  'node_id' => 	$node->nid,
					  'term_name' => $node_term->name,
					  'packshot_name' => $node_title->field_packshot_nmae['und'][0]['value'],
					  'formula_desc' => $node_title->field__find_formula_description['und'][0]['value'],
					  'title' => $node_title->title,
					);
			}	
	}	
	?>
  <body>
   <nav class="navbar navbar-default">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".nav-collapse" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
		<!--<button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar tb-megamenu-button menuIstance-processed" type="button">
      <i class="fa fa-reorder" ></i>
    </button>-->
		<?php if ($logo): ?>
				<a class="navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Mens Health'); ?>" rel="home" id="logo">
				<img src="<?php print $logo; ?>" alt="<?php print t('Mens Health'); ?>" />        </a>
				<?php endif; ?> 
      </div>
		<!-- Collect the nav links, forms, and other content for toggling -->
      <div class="nav-collapse collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-left nav-bg">
          <?php print render($page['header']); ?>
        </ul>
      </div><!-- /.navbar-collapse --><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
    </nav>
	<?php print render($page['custom_menu']); ?>
<div class="products-banner"><img src="<?php print $vitamin_image_url; ?>">
  <div class="products-bannertext">
    <p><img src="<?php print $path_to_theme . '/images/mens-health-ban-logo.png'; ?>" title="Men's Health Lab" /></p>	
    <h1><?php print t('VITAMIN SELECTOR');?></h1>
  </div>
</div>
    <div class="container">

      <div class="row">
        <div class="prod-disp">
          <div class="col-sm-4 vitamin-intro-text">
            <p class="prod-introduction"><?php echo variable_get('left-text'); ?></p>
          </div>
          <div class="col-sm-8 vitamin-intro-text">
            <p><?php echo variable_get('right-text'); ?></p>
            <div class="">
              <p class="select-benefit">Select your benefit</p>
              <p class="select-benefit select-benefit-arrow"></p>
            </div>
          </div>
		   <div class="clearfix"></div>
		<?php foreach($vitamins as $key=>$value): ?>
            <div class="col-sm-4">
            <div class="product-benefits">
			
              <p class="products-bg <?php echo $value[vitamin_name]; ?>" id='<?php echo $value[term_id];?>'><?php echo $value[vitamin_name]; ?></p>
              <p class="products-icon <?php echo $value[vitamin_name]; ?> normal" id='<?php echo $value[term_id];?>'><img src="<?php echo $value[vitamin_image]; ?>"/></p>
			  <div class="vitamin-selector">
			  <p class="products-icon <?php echo $value[vitamin_name]; ?>" id='<?php echo $value[term_id];?>'><img src="<?php echo $value[vitamin_active_image]; ?>"/></p>
			  </div>
            </div>
          </div>
		  <?php endforeach; ?>
        </div>
      </div>
    </div>
	<script type="text/javascript" src="<?php print $path_to_theme . '/js/formula/formula_landing.js'; ?>"></script>
	 <div id="ajax-data"></div>	 
	<?php if ($page['footer']): ?>
	<footer>
	<div class="container">
      <div class="row">
        <?php print render($page['footer']); ?>
      </div>
    </div>
	</footer>
 <!-- /#footer -->
    <?php endif; ?>
	
	
	
	


