<?php 
/**
 * @file
 * Default theme implementation to display a single Drupal page.
 *
 * The doctype, html, head and body tags are not in this template. Instead they
 * can be found in the html.tpl.php template in this directory.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see html.tpl.php
 *
 * @ingroup themeable
 */
 global $user;
 global $base_url;
 $path_to_theme =  $base_url . '/' . path_to_theme();

//print_r($main_menu->title);
?>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".nav-collapse" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
		<!--<button data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar tb-megamenu-button menuIstance-processed" type="button">
      <i class="fa fa-reorder" ></i>
    </button>-->
		<?php if ($logo): ?>
				<a class="navbar-brand" href="<?php print $front_page; ?>" title="<?php print t("Men's Health Lab"); ?>" rel="home" id="logo">
				<img src="<?php print $logo; ?>" alt="<?php print t("Men's Health Lab"); ?>" />        </a>
				<?php endif; ?> 
      </div>
		<div class="nav-collapse collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-left nav-bg">
          <?php print render($page['header']); ?>
        </ul>
      </div><!-- /.navbar-collapse --><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
    </nav>
	<?php print render($page['custom_menu']); ?>
	<?php if ($site_name || $site_slogan): ?>
      <div id="name-and-slogan"<?php if ($hide_site_name && $hide_site_slogan) { print ' class="element-invisible"'; } ?>>

        <?php if ($site_name): ?>
          <?php if ($title): ?>
            <div id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>>
              <strong>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
              </strong>
            </div>
          <?php else: /* Use h1 when the content title is empty */ ?>
            <h1 id="site-name"<?php if ($hide_site_name) { print ' class="element-invisible"'; } ?>>
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
            </h1>
          <?php endif; ?>
        <?php endif; ?>

        <?php if ($site_slogan): ?>
          <div id="site-slogan"<?php if ($hide_site_slogan) { print ' class="element-invisible"'; } ?>>
            <?php print $site_slogan; ?>
          </div>
        <?php endif; ?>

      </div> <!-- /#name-and-slogan -->
    <?php endif; ?>
	
    <div class="container-fluid">
      <div class="row">
	  <a href="<?php echo $base_url; ?>/products">
          <div class="home-banner">
		  <?php print render($page['home_banner']); ?>
            <div class="home-banner-text">
			<p><img src="<?php print $path_to_theme . '/images/new.png'; ?>" title="Men's Health Lab" /></p>
              <p><img src="<?php print $path_to_theme . '/images/mens-health-ban-logo.png'; ?>" title="Men's Health Lab" /></p>			  
              <?php print t('<h1>THE ULTIMATE VITAMINS<br>RANGE FOR MEN'); ?></h1>
              <h3>#RELEASE<span style="color: #d80f23">YOUR</span>BEST</h3>         
            </div>
        </div>
		</a>
      </div>
    </div>
	<div class="container">
      <div class="row">
        <div class="column-1 col-sm-4">
		<a href="<?php echo $base_url; ?>/vitamin-selector">
          <div class="products">
            <div class="prducts-head">
              <h4><?php print t('FIND YOUR'); ?></h4><h1><?php print t('FORMULA'); ?></h1>
            </div>
			<?php print render($page['triptych_first']); ?>  
            <a href="<?php echo $base_url; ?>/vitamin-selector"><button class="btn btn-click"><span class="btn-click-cont"><?php print t('START HERE'); ?></span></button></a>
          </div>
		  </a>
        </div>
        <div class="column-2 col-sm-4">
		    <div class="products">
			<?php print render($page['triptych_middle']); ?>
            <a href="<?php echo $base_url; ?>/products"><button class="btn btn-click"><span class="btn-click-cont"><?php print t('FULL RANGE'); ?></span></button><a/>
          </div>		  
        </div>
        <div class="column-3 col-sm-4">
          <div class="products">
		  <!--<a href="<?php //echo $base_url; ?>/content/ultimate-vitamin-range-men">-->
		    <div class="about-head">
			   <!--<h1><?php //print t('ABOUT'); ?></h1>-->
              <!--<img src="<?php //print $path_to_theme . '/images/mens-health-ban-logo.png'; ?>" />-->
            </div>
			<!--</a>-->
			<?php print render($page['triptych_last']); ?>
            <a href="<?php echo $base_url; ?>/content/ultimate-vitamin-range-men"><button class="btn btn-click"><span class="btn-click-cont"><?php print t('EXPLORE'); ?></span></button></a>
          </div>
        </div>
      </div>
    </div>
	<footer>
	<div class="container">
      <div class="row">
        <?php print render($page['footer']); ?>
      </div>
    </div>
	</footer>
	</body>