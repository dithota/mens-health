<?php
/**
 * @file node.tpl.php
 *
 * Theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: Node body or teaser depending on $teaser flag.
 * - $picture: The authors picture of the node output from
 *   theme_user_picture().
 * - $date: Formatted creation date (use $created to reformat with
 *   format_date()).
 * - $links: Themed links like "Read more", "Add new comment", etc. output
 *   from theme_links().
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $terms: the themed list of taxonomy term links output from theme_links().
 * - $submitted: themed submission information output from
 *   theme_node_submitted().
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $teaser: Flag for the teaser state.
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 */
 ?>
 <?php
 //print_r($node);
 global $base_url;
 $path_to_theme =  $base_url . '/' . path_to_theme();
  if($node->field_vendors['und'][0]['value']!='') {
 $store_url = db_query("select field_vendor_tid as vendorid, field_store_url_value as url from field_data_field_vendor v, field_data_field_store_url s where s.entity_id=v.entity_id and v.entity_id=".$node->field_vendors['und'][0]['value'])->fetchAll();
  }
  ?>
<div class="products-banner"><img src="<?php print render (file_create_url($node->field_product_header_image['und'][0]['uri'])); ?>">
  <div class="products-bannertext">
    <p><img src="<?php print $base_url . '/profiles/mens_health/themes/custom/mens_health/images/mens-health-ban-logo.png'; ?>" title="Men's Health Lab" /></p>	
    <h1><?php print $node->field_packshot_nmae['und'][0]['value']; ?></h1>
  </div>
</div>

 <div class="container">
      <div class="row">
	  <div class="prod-disp">
        <div class="prod-details">
          <div class="col-sm-5 text-center">
           <div class="prod-desc">
                
			<?php
				foreach($store_url as $value)
				{
				$term = taxonomy_term_load($value->vendorid);
			?>
				<a href="<?php echo $value->url; ?>" target="_blank">
				<div class="prod_img"><img src="<?php print render (file_create_url($node->field_product_image['und'][0]['uri'])); ?>" /></div>
				<button class="btn btn-click <?php print $node->field_packshot_nmae['und'][0]['value']; ?>">				
				<p class="btn-click-cont-yello"></p>
				<span class="btn-click-cont"><?php print t('BUY NOW'); ?></span>
			</button>
			</a>
			<?php } ?>

              </div>
          </div>
          <div class="col-sm-6 prod-intro-center">
		  <p class="prod-introduction <?php print $node->field_packshot_nmae['und'][0]['value']; ?>"><?php print $node->field_introduction['und'][0]['value']; ?></p>
            <?php print $node->body['und'][0]['value']; ?>	
          </div>
        </div>
      </div>
	  </div>
    </div>
	<div class="container">
      <div class="row">
        <div class="prod-disp">
          <div class="col-sm-12">
            <div class="formulated active">
              <p><span><?php print t('Specially formulated to support'); ?></span></p>

            </div>
            <div class="whatsinit">
              <p><span><?php print t('what is in it'); ?></span></p>
            </div>
            <div class="formulated-details">
              <div class="row">
          <div class="col-sm-12">
			<?php print $node->field_product_benefits['und'][0]['value']; ?>
          </div>
		</div>
            </div>
            <div class="whatsinit-details">
              <div class="row">
        <div class="col-sm-12">
	  <?php print $node->field_ingradient_table['und'][0]['value']; ?>
	  </div>
	  </div>
	    <div class="row">
          <div class="col-sm-12">
		  <p><strong>Ingredients:</strong></p>
          <?php print $node->field_ingredients['und'][0]['value']; ?>
		  <p><?php print $node->field_ingradient_description['und'][0]['value']; ?></p>
          </div>
		</div>
            </div>
          </div>
        </div>
      </div>
    </div>
	<?php if ($page['footer']): ?>
	<footer>
        <?php print render($page['footer']); ?>
		</footer>
 <!-- /#footer -->
    <?php endif; ?>