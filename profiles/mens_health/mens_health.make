api = 2
core = 7.x
; Include the definition for how to build Drupal core directly, including patches:
includes[] = drupal-org-core.make

projects[mens_health][type] = profile
projects[mens_health][download][type] = git
projects[mens_health][download][url] = git@bitbucket.org:omegapharma/omega_xls_com.git
projects[mens_health][download][branch] = master
projects[mens_health][subdir] = ""