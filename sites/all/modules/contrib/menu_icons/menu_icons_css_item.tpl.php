<?php

/**
 * @file
 *
 * Template file for generating the CSS file used for the menu-items
 */

/**
 * Variables:
 * $mlid
 * $path
 */
?>

<?php global $base_url;?>
a.menu-<?php print $mlid ?>, ul.links li.menu-<?php print $mlid ?> a {
  background-image: url(<?php echo $base_url.$path ?>) !important;
  padding-<?php print "$pos:$size"?>px;
  background-repeat: no-repeat !important;
  background-position: <?php print $pos?>;
  height: <?php print $height?>px;
}